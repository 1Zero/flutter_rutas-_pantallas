
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Home extends StatefulWidget {
 static const String ROUTE = "/";

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          primary: true,
          slivers: [
            SliverAppBar(
              title: Row(
                children: [
                  Image.asset('assets/img/youtube.png',
                  height: 50,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text('YouTube'),
                ],
              ),
              actions: [
                IconButton(
                  onPressed: (){}, 
                  icon: Icon(Icons.tv)
                ),
                IconButton
                (onPressed: (){}, 
                icon: Icon(FontAwesomeIcons.bell)
                ),
                IconButton(
                  onPressed: (){}, 
                  icon: Icon(FontAwesomeIcons.search)
                  ),
                 GestureDetector(
                   child: CircleAvatar(
                     radius:15,
                     backgroundImage: AssetImage
                     ('assets/img/zero2.jpg'),
                  ),
                  onTap: (){},
                 ), 
              ],
            ),
            
          ],

        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
             BottomNavigationBarItem(
               icon: Icon(FontAwesomeIcons.home),
               label: "principal",
            ),
             BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.compass),
              label: "explora",
            ),
             BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.plusCircle),
              label: "",
            ),
             BottomNavigationBarItem(
              icon: Icon(Icons.subscriptions_rounded),
              label: "suscripciones",
            ),
             BottomNavigationBarItem(
              icon: Icon(Icons.library_add_check_outlined),
              label: "biblioteca",
            ) 
          ],
        ),
        
    
      ),
    );
  }
}