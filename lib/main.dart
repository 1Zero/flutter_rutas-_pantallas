import 'package:flutter/material.dart';

import 'features/practica/presentation/pages/home_page.dart';
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      initialRoute: Home.ROUTE,
      routes: {
        Home.ROUTE:(_)=>Home(),

      }
    );
  }
}